import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'po',
    loadChildren: () => import('./po/po.module')
      .then(m => m.POModule),
  },
  { path: '', redirectTo: 'po', pathMatch: 'full' },
  { path: '**', redirectTo: 'po' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
