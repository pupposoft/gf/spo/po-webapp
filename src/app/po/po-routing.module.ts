import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { POComponent } from './po.component';
import { ScenarioComponent } from './scenario/scenario.component';

const routes: Routes = [{
  path: '',
  component: POComponent,
  children: [
    {
      path: 'scenario',
      component: ScenarioComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PORoutingModule {
}
