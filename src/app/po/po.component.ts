import { Component } from '@angular/core';

import { MENU_ITEMS } from './po-menu';

@Component({
  selector: 'ngx-po-component',
  styleUrls: ['po.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class POComponent {

  menu = MENU_ITEMS;
}
