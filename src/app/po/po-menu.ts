import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Cenários',
    icon: 'shopping-cart-outline',
    link: '/po/scenario',
    home: true,
  },
];
