import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { POComponent } from './po.component';
import { PORoutingModule } from './po-routing.module';

@NgModule({
  imports: [
    PORoutingModule,
    ThemeModule,
    NbMenuModule,
  ],
  declarations: [
    POComponent,
  ],
})
export class POModule {
}
