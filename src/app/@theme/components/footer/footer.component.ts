import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Feito com ♥ by PuppoSoft 2020
    </span>
    <div class="socials">
      <a href="https://github.com/renandpf" target="_blank" class="ion ion-social-github"></a>
      <a href="https://www.linkedin.com/in/renan-furtado/" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
